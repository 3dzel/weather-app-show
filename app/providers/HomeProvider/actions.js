/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this weatherApp uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  LOAD_WEATHER,
  LOAD_WEATHER_SUCCESS,
  LOAD_WEATHER_ERROR,
} from './constants';

/**
 * Get weather
 * */
export function loadWeather() {
  return {
    type: LOAD_WEATHER,
  };
}

export function weatherLoaded(res, weather) {
  return {
    type: LOAD_WEATHER_SUCCESS,
    res,
    weather,
  };
}

export function weatherLoadingError(error) {
  return {
    type: LOAD_WEATHER_ERROR,
    error,
  };
}
