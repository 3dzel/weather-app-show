/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';
import { makeSelectDegrees, makeSelectCityId } from 'containers/App/selectors';
import { makeSelectLocale } from 'providers/LanguageProvider/selectors';
import { APPID } from 'utils/constants';
import { LOAD_WEATHER } from './constants';
import { weatherLoaded, weatherLoadingError } from './actions';
import { makeSelectWeather } from './selectors';

/**
 * Github repos request/response handler
 */
export function* getWeather() {
  // Select username from store
  const weather = yield select(makeSelectWeather());
  const degrees = yield select(makeSelectDegrees());
  const language = yield select(makeSelectLocale());
  const cityId = yield select(makeSelectCityId());

  const requestURL = `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&APPID=${APPID}&lang=${language}&units=${degrees}`;

  try {
    // Call our request helper (see 'utils/request')
    const res = yield call(request, requestURL);
    yield put(weatherLoaded(res, weather));
  } catch (err) {
    yield put(weatherLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* weatherSaga() {
  yield takeLatest(LOAD_WEATHER, getWeather);
}
