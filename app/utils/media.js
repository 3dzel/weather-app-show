import { css } from 'styled-components';

// export const sizes = {
//   desktop: 1199,
//   tablet: 899,
//   phone: 599,
// };

export const sizesMax = {
  tabletLandscape: 1199,
  tabletPortrait: 899,
  phone: 599,
};

/*  ${maxMedia.название устройства -   указанное устройство и все что меньше
 * ${maxMedia.tabletLandscape` background: green;`} = @media (max-width: 73.125em) { background: green;} // 73.125em = 1170px !Включительно последний пиксель !
 *  0px                      320px                  599px                 899px              1199               ∞
 * ╔══════════════╦═════════════╦═══════════╦═══════════╦═════════════════════════
 * ╠
 * ╠                           ←   ${maxMedia.tabletLandscape   →                             ╣
 * ╠                    ←  ${maxMedia.tabletPortrait   →                 ╣
 * ╠              ← ${maxMedia.phone   →            ╣
 *  */
export const maxMedia = Object.keys(sizesMax).reduce((accumulator, label) => {
  const emSize = sizesMax[label] / 16;

  // eslint-disable-next-line no-param-reassign
  accumulator[label] = (...args) => css`
    @media (max-width: ${emSize}em) {
      ${css(...args)}
    }
  `;

  return accumulator;
}, {});

export const sizesMin = {
  desktop: 1200,
  tabletLandscape: 900,
  tabletPortrait: 600,
  phone: 320,
};

/*   ${minMedia.название устройства -    УКАЗАННОЕ УСТРОЙСТВО и все что болльше него
 * ${minMedia.desktop`background: blue;`} = @media (min-width: 73.125em) { background: green;} // 73.125em = 1170px !Включительно первый пиксель 1170!
 *  0px                      320px                  600px                 900px               1200               ∞
 * ╔══════════════╦═════════════╦═══════════╦═══════════╦═════════════════════════
 * ╠                                                                                           ╠       ←  ${minMedia.desktop  →
 * ╠                                                                      ╠    ←   ${minMedia.tabletLandscape   →
 * ╠                                                 ╠ ←  ${minMedia.tabletPortrait   →
 * ╠                         ╠ ← ${minMedia.phone   →
 *  */

export const minMedia = Object.keys(sizesMin).reduce((accumulator, label) => {
  const emSize = sizesMin[label] / 16;

  // eslint-disable-next-line no-param-reassign
  accumulator[label] = (...args) => css`
    @media (min-width: ${emSize}em) {
      ${css(...args)}
    }
  `;

  return accumulator;
}, {});

/*        ${media.название устройства -    ТОЛЬКО УКАЗАННОЕ УСТРОЙСТВО
 * ${media.tabletPortrait`background: blue;`} =@media (max-width: 56.1875em) and (min-width: 37.5em) { background: blue;}
 *  0px             320px         599px 600px                 899px 900px                     1199px  1200px              ∞
 * ╔═════════╦═════════╦╦══════════════╦╦═════════════════╦╦═════════════════════════
 * ╠
 * ╠                                                               ╠ ← ${media.tabletLandscape → ╣
 * ╠                                   ╠←${media.tabletPortrait→╣
 * ╠               ╠←${media.phone→╣
 *  */

export const media = Object.keys(sizesMax).reduce((accumulator, label) => {
  const emSizeMax = sizesMax[label] / 16;
  let emSizeMin;

  switch (label) {
    case 'tabletPortrait':
      emSizeMin = sizesMin.tabletPortrait / 16;
      break;
    case 'tabletLandscape':
      emSizeMin = sizesMin.tabletLandscape / 16;
      break;
    default:
      emSizeMin = sizesMin.phone / 16;
  }

  // eslint-disable-next-line no-param-reassign
  accumulator[label] = (...args) => css`
    @media (min-width: ${emSizeMin}em) and (max-width: ${emSizeMax}em) {
      ${css(...args)}
    }
  `;

  return accumulator;
}, {});
