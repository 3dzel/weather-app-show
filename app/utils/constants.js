export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const DEGREES = [{ id: 'metric' }, { id: 'imperial' }];
export const CITIES = ['1496153', '1520172'];

export const APPID = `ca8f8af085b9ac047d073bf5938e4194`;
