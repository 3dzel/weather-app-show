import { defineMessages } from 'react-intl';

export const scope = 'weatherApp.utils.cityMessages';

export default defineMessages({
  1496153: {
    id: `${scope}.1496153`,
    defaultMessage: 'Omsk',
  },
  1520172: {
    id: `${scope}.1520172`,
    defaultMessage: 'Petropavlovsk',
  },
});
