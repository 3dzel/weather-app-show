import styled from 'styled-components';

const Select = styled.select`
  color: #fff;
  font-size: 20px;
  line-height: 1em;
  background-color: transparent;
  border-style: none;
  > option {
    color: #333;
  }
`;

export default Select;
