import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 2em auto;
  width: 60px;
  height: 60px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export default Wrapper;
