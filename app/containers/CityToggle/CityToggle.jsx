import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { changeCity } from 'containers/App/actions';
import { makeSelectCityId } from 'containers/App/selectors';
import Toggle from 'components/Toggle';

import { CITIES } from 'utils/constants';
import messages from './messages';
import { loadWeather } from '../../providers/HomeProvider/actions';

export function DegreesToggle(props) {
  return (
    <Toggle
      value={props.cityId}
      values={CITIES}
      messages={messages}
      onToggle={e => {
        props.onCityChange(e);
        props.getWeather();
      }}
    />
  );
}

DegreesToggle.propTypes = {
  onCityChange: PropTypes.func,
  getWeather: PropTypes.func,
  cityId: PropTypes.string,
};

const mapStateToProps = createSelector(
  makeSelectCityId(),
  cityId => ({
    cityId,
  }),
);

export function mapDispatchToProps(dispatch) {
  return {
    onCityChange: e => dispatch(changeCity(e.target.value)),
    getWeather: () => dispatch(loadWeather()),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DegreesToggle);
