import RainImg from './img/weather/rain.svg';
import ClearImg from './img/weather/clear.svg';
import CloudsImg from './img/weather/clouds.svg';
import ThunderstormImg from './img/weather/thunderstorm.svg';

export default {
  Clear: ClearImg,
  Rain: RainImg,
  Clouds: CloudsImg,
  Thunderstorm: ThunderstormImg,
};
