/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import DegreesToggle from 'containers/DegreesToggle';
import CityToggle from 'containers/CityToggle';
import LoadingIndicator from 'components/LoadingIndicator';
import LocaleToggle from 'containers/LocaleToggle';
import cityMessages from 'utils/cityMessages';

import { loadWeather } from '../../providers/HomeProvider/actions';
import { makeSelectWeather } from '../../providers/HomeProvider/selectors';

import gpsImg from './img/gps.svg';
import iconsList from './iconsList';
import messages from './messages';

import {
  Wrapper,
  HeaderRow,
  CityTitle,
  Label,
  GpsImg,
  Board,
  BoardRow,
  BoardImg,
  BoardDegree,
  BoardText,
  Footer,
  FooterItem,
} from './styled';

class HomePage extends React.Component {
  componentWillMount() {
    this.props.getWeather();
  }

  render() {
    if (this.props.weather.loading) return <LoadingIndicator />;

    const { weather } = this.props.weather;
    const { id } = weather;
    const { humidity } = weather.main;
    const { pressure } = weather.main;
    const temp = Math.round(weather.main.temp);
    const { description } = weather.weather[0];
    const { main } = weather.weather[0];
    const windSpeed = weather.wind.speed;
    const clouds = weather.clouds.all;

    return (
      <article>
        <Helmet>
          <title>Weather App</title>
          <meta name="Weather App" content="This is weather application" />
        </Helmet>
        <Wrapper>
          <header>
            <HeaderRow>
              <CityTitle>
                <FormattedMessage {...cityMessages[id]} />
              </CityTitle>
              <DegreesToggle />
            </HeaderRow>
            <HeaderRow>
              <Label>
                <FormattedMessage {...messages.changeCity} />
                <CityToggle />
              </Label>
              <Label location>
                <GpsImg src={gpsImg} alt="location" />
                <FormattedMessage {...messages.myLocation} />
              </Label>
            </HeaderRow>
            <LocaleToggle />
          </header>
          <main>
            <Board>
              <BoardRow>
                <BoardImg src={iconsList[`${main}`]} />
                <BoardDegree>{temp}</BoardDegree>
              </BoardRow>
              <BoardText>{description}</BoardText>
            </Board>
          </main>
          <Footer>
            <FooterItem>
              <Label>
                <FormattedMessage {...messages.wind} />
              </Label>
              <p>{`${windSpeed} м/c`}</p>
            </FooterItem>
            <FooterItem>
              <Label>
                <FormattedMessage {...messages.pressure} />
              </Label>
              <p>{`${pressure} мм рт.ст.`}</p>
            </FooterItem>
            <FooterItem>
              <Label>
                <FormattedMessage {...messages.humidity} />
              </Label>
              <p>{`${humidity}%`}</p>
            </FooterItem>
            <FooterItem>
              <Label>
                <FormattedMessage {...messages.clouds} />
              </Label>
              <p>{`${clouds}%`}</p>
            </FooterItem>
          </Footer>
        </Wrapper>
      </article>
    );
  }
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  getWeather: PropTypes.func,
  weather: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  weather: makeSelectWeather(),
});

export function mapDispatchToProps(dispatch) {
  return {
    getWeather: () => dispatch(loadWeather()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
