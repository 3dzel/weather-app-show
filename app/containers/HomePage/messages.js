/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'weatherApp.containers.HomePage';

export default defineMessages({
  myLocation: {
    id: `${scope}.myLocation`,
    defaultMessage: 'My location',
  },
  wind: {
    id: `${scope}.wind`,
    defaultMessage: 'Wind',
  },
  pressure: {
    id: `${scope}.pressure`,
    defaultMessage: 'Pressure',
  },
  humidity: {
    id: `${scope}.humidity`,
    defaultMessage: 'Humidity',
  },
  clouds: {
    id: `${scope}.clouds`,
    defaultMessage: 'Clouds',
  },
  changeCity: {
    id: `${scope}.changeCity`,
    defaultMessage: 'Change City',
  },
});
