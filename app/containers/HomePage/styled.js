import styled from 'styled-components';
import { maxMedia } from 'utils/media';

export const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  padding: 4%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  ${maxMedia.phone`
    padding: 20px 20px 30px;
  `}
`;

export const HeaderRow = styled.div`
  margin-bottom: 12px;
  display: flex;
  justify-content: space-between;
`;

export const CityTitle = styled.span`
  font-size: 27px;
  line-height: 1.3em;
`;

export const Label = styled.label`
  font-size: 14px;
  opacity: 0.5;
  display: ${props => props.location && 'none'};

  ${maxMedia.tabletLandscape`
    display: ${props => props.location && 'inline-block'};
  `}
`;

export const GpsImg = styled.img`
  width: 16px;
  position: relative;
  left: -10px;
  top: -2px;
`;
export const Board = styled.div`
  text-align: center;
`;
export const BoardRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const BoardImg = styled.img`
  margin: 10px;
  width: 100px;
`;
export const BoardDegree = styled.span`
  position: relative;
  font-size: 120px;

  ${maxMedia.phone`
    font-size: 72px;
  `}

  &:before {
    content: 'O';
    position: absolute;
    right: -28px;
    top: -2px;
    font-size: 36px;
  }
`;
export const BoardText = styled.p`
  font-size: 18px;
`;
export const Footer = styled.footer`
  margin: 0 -10px;
  display: flex;
  flex-wrap: wrap;
`;
export const FooterItem = styled.div`
  padding: 10px;
  width: 25%;
  > p {
    margin: 0;
  }

  ${maxMedia.phone`
    width: 50%;
  `}
`;
