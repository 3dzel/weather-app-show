import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import { changeDegrees } from 'containers/App/actions';
import { makeSelectDegrees } from 'containers/App/selectors';

import { DEGREES } from 'utils/constants';
import messages from './messages';
import { loadWeather } from '../../providers/HomeProvider/actions';

const List = styled.ul`
  position: relative;
  margin: 6px 24px;
  padding: 0;
  border: 1px solid #fff;
  border-radius: 8px;
  display: flex;
  list-style-type: none;
  &:before {
    content: '\\00b0';
    position: absolute;
    left: -18px;
    top: -2px;
    font-size: 24px;
  }
`;

const ListItem = styled.li`
  padding: 1px 14px;
  cursor: ${props => !props.active && 'pointer'};
  font-weight: ${props => props.active && 'bold'};
  background-color: ${props => props.active && 'rgba(255,255,255,.2)'};
`;

export function DegreesToggle(props) {
  return (
    <List>
      {DEGREES.map(item => (
        <ListItem
          key={item.id}
          active={props.degrees === item.id}
          onClick={() => {
            props.onDegreesToggle(item.id);
            props.getWeather();
          }}
        >
          {<FormattedMessage {...messages[item.id]} />}
        </ListItem>
      ))}
    </List>
  );
}

DegreesToggle.propTypes = {
  onDegreesToggle: PropTypes.func,
  getWeather: PropTypes.func,
  degrees: PropTypes.string,
};

const mapStateToProps = createSelector(
  makeSelectDegrees(),
  degrees => ({
    degrees,
  }),
);

export function mapDispatchToProps(dispatch) {
  return {
    onDegreesToggle: degree => dispatch(changeDegrees(degree)),
    getWeather: () => dispatch(loadWeather()),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DegreesToggle);
