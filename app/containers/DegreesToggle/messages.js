/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'weatherApp.containers.DegreesToggle';

export default defineMessages({
  metric: {
    id: `${scope}.metric`,
    defaultMessage: 'С',
  },
  imperial: {
    id: `${scope}.imperial`,
    defaultMessage: 'F',
  },
});
